package com.madeinworld.android.firebase;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by janos on 10/10/2016.
 */
@IgnoreExtraProperties
public class FBUser {
    public static final String FIREBASE_KEY_STORE_NAME = "storeName";
    public static final String FIREBASE_KEY_USERNAME = "userName";
    public static final String FIREBASE_KEY_USER_ID = "sapID";


    public String thumbnail;

    public Map<String, Boolean> profileImages = new HashMap<>();
    public Map<String, Boolean> publications = new HashMap<>();
    public String chineseStoreName;


    private String storeName;
    private String sapID;
    private String userName;

    private String firebaseID;

    public FBUser(){
        // Default constructor required for calls to DataSnapshot.getValue(Publication.class)
    }

    public FBUser(String storeName, String sapID, String userName){
        this.storeName = storeName;
        this.sapID = sapID;
        this.userName = userName;
    }

    public FBUser(String storeName, String sapID, String chineseStoreName, Map<String, Boolean> profileImages, Map<String, Boolean> publications, String thumbnail) {
        this.sapID = sapID;
        this.storeName = storeName;
        this.profileImages = profileImages;
        this.thumbnail = thumbnail;
        this.chineseStoreName = chineseStoreName;
        this.publications = publications;
    }



    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String toString(){
        StringBuffer buf = new StringBuffer();
        buf.append("FBUSer[");
        buf.append("]" );
        if (storeName != null) {
            buf.append(storeName);
        }
        buf.append("sapID=");
        buf.append(sapID);
        return buf.toString();
    }

    public void setSapID(String sapID) {
        this.sapID = sapID;
    }

    public String getSapID(){
        return sapID;
    }

    public Map<String, String> getAsFirebaseNode() {
        Map<String, String> fbUserNode = new HashMap<>();
        fbUserNode.put(FIREBASE_KEY_STORE_NAME, storeName);
        fbUserNode.put(FIREBASE_KEY_USERNAME, userName);
        fbUserNode.put(FIREBASE_KEY_USER_ID, sapID);
        return fbUserNode;
    }

    public String getProfileImageThumbnailURL() {
        if(profileImages != null) {
            Map.Entry<String, Boolean> entry = profileImages.entrySet().iterator().next();
            String imagekey = entry.getKey();
            return imagekey;
        }
        return null;
    }

    public List<String> getPublicationKeyList() {

        List<String> list = new ArrayList<String>(publications.keySet());

        return list;
    }


    public String getFirebaseID() {
        return firebaseID;
    }

    public void setFirebaseID(String firebaseID) {
        this.firebaseID = firebaseID;
    }
}

