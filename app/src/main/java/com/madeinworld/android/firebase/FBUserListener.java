package com.madeinworld.android.firebase;

public interface FBUserListener {
    void userFound(FBUser firebaseUser);
    void userNotFound(String returnedJson);
    void userListenerError();
}

