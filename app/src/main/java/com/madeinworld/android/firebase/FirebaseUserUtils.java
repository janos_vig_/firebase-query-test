package com.madeinworld.android.firebase;

import java.util.Map;


import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.madeinworld.android.firebase.FBUser.FIREBASE_KEY_USER_ID;


public class FirebaseUserUtils {

    public static final String FIREBASE_USERS_URL = "users";

    private static FirebaseUserUtils instance;

    private Boolean isUpdateInProgress = false;

    private FirebaseUserUtils() {
    }

    public static synchronized FirebaseUserUtils getInstance() {
        if (instance == null) {
            instance = new FirebaseUserUtils();
        }
        return instance;
    }


    public void createStore(FBUser fbUser, DatabaseReference.CompletionListener listener) {
        DatabaseReference firebaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersDatabase = FirebaseDatabase.getInstance().getReference().child(FIREBASE_USERS_URL);
        String newStoreFirebaseKey = firebaseReference.child(FIREBASE_USERS_URL).push().getKey();
        Map<String, String>  storeAsFirebaseNode = fbUser.getAsFirebaseNode();
        usersDatabase.child(newStoreFirebaseKey).setValue(storeAsFirebaseNode, listener);
    }



    public void createStoreInFirebaseIfNotExists(final FBUser store) {
        Log.d(FirebaseConstants.TAG, "FirebaseUserUtils.createStoreInFirebaseIfNotExists: "  + store);
        if(isUpdateInProgress){
            Log.d(FirebaseConstants.TAG, "FirebaseUtils.createStoreInFirebaseIfNotExists: isUpdateProgress" + isUpdateInProgress);
            return;
        }
        isUpdateInProgress = true;

        retrieveFBUserBySapID_singleValueListener(store.getSapID(), new FBUserListener() {

            @Override
            public void userFound(FBUser firebaseUser) {
                Log.d(FirebaseConstants.TAG, "FirebaseUtils.createStoreInFirebaseIfNotExists.userFound: " + firebaseUser);
                isUpdateInProgress = false;
            }

            @Override
            public void userNotFound(String returnedJson) {
                Log.d(FirebaseConstants.TAG, "FirebaseUtils.createStoreInFirebaseIfNotExists.userNotFound: ");
                createStore(store, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError == null) {
                            Log.d(FirebaseConstants.TAG, "FirebaseUserUtils.CreateStore.onComplete: ");
                        }else{
                            Log.e(FirebaseConstants.TAG, "FirebaseUserUtils.CreateStore.onComplete: error while creating" + databaseError);
                        }
                        isUpdateInProgress = false;
                    }
                });
            }

            @Override
            public void userListenerError() {
                Log.d(FirebaseConstants.TAG, "FirebaseUtils.createStoreInFirebaseIfNotExists.userListenerError: ");
                isUpdateInProgress = false;
            }
        });
    }



    public void retrieveFBUserBySapID_singleValueListener(final String userID, final FBUserListener fbUserListener) {
        Log.d(FirebaseConstants.TAG, "FirebaseUserUtils.retrieveFBUserBySapID_singleValueListener: " + userID);
        DatabaseReference usersDatabase = FirebaseDatabase.getInstance().getReference().child(FIREBASE_USERS_URL);
        usersDatabase.orderByChild(FIREBASE_KEY_USER_ID).equalTo(userID).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(FirebaseConstants.TAG, "FirebaseUserUtils.onDataChange.retrieveFBUserBySapID_singleValueListener: " + dataSnapshot);
                        if (dataSnapshot.exists()) {

                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                FBUser fbUser = childDataSnapshot.getValue(FBUser.class);
                                fbUserListener.userFound(fbUser);
                            }
                        } else {
                            fbUserListener.userNotFound(dataSnapshot.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        fbUserListener.userListenerError();
                    }
                }
        );
    }
    
    private void retrieveUserByID_usingTwoListeners(final String userID, final FBUserListener fbUserListener) {
        final DatabaseReference usersDatabase = FirebaseDatabase.getInstance().getReference().child(FIREBASE_USERS_URL);
        usersDatabase.orderByChild(FIREBASE_KEY_USER_ID).equalTo(userID).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                FBUser fbUser = childDataSnapshot.getValue(FBUser.class);
                                fbUserListener.userFound(fbUser);
                            }


                        } else {
                            fbUserListener.userNotFound( dataSnapshot.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        fbUserListener.userListenerError();
                    }
                }
        );

        //Seems that without this listener(childeventlistener) the previous listener(eventlistener) is not working correctly but it results in not found even if the node with the given userID is existent.
        usersDatabase.orderByChild(FIREBASE_KEY_USER_ID).equalTo(userID).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                usersDatabase.removeEventListener(this);
                FBUser fbUser = dataSnapshot.getValue(FBUser.class);
                fbUserListener.userFound(fbUser);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
