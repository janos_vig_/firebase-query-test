package com.yunnyyy.firebasequerytest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.madeinworld.android.firebase.FBUser;
import com.madeinworld.android.firebase.FBUserListener;
import com.madeinworld.android.firebase.FirebaseConstants;
import com.madeinworld.android.firebase.FirebaseUserUtils;

public class MainActivity extends AppCompatActivity {



    TextView textViewResults = null;
    FBUser storeToTest = new FBUser("Test StoreName", "0200000262" ,  "testusername");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResults = (TextView) findViewById(R.id.textViewResults);


        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Button buttonTestOK = (Button) findViewById(R.id.button_testok);
        buttonTestOK.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                textViewResults.setText("[TEST OK]Searching for user with ID=" + storeToTest.getSapID());
                executeTestOK();
            }
        });


        Button buttonTestNotOK = (Button) findViewById(R.id.button_test_not_ok);
        buttonTestNotOK.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                textViewResults.setText("[TEST NOT OK]Searching for user with ID=" + storeToTest.getSapID());
                executeFailingTest();
            }
        });


    }

    private void executeFailingTest(){
        FirebaseUserUtils.getInstance().createStoreInFirebaseIfNotExists(storeToTest);

    }



    private void executeTestOK(){
        FirebaseUserUtils.getInstance().retrieveFBUserBySapID_singleValueListener(storeToTest.getSapID(), new FBUserListener() {
            @Override
            public void userFound(FBUser FBUser) {
                Log.d(FirebaseConstants.TAG, "MainActivity.executeTestOK.userFound: ");
            }

            @Override
            public void userNotFound(String returnedJson) {
                Log.d(FirebaseConstants.TAG, "MainActivity.executeTestOK.userNotFound: ");
                FirebaseUserUtils.getInstance().createStore(storeToTest, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Log.d(FirebaseConstants.TAG, "DataContext.onComplete: Store created in Firebase");
                    }
                });
            }

            @Override
            public void userListenerError() {
                Log.d(FirebaseConstants.TAG, "MainActivity.executeTestOK.userListenerError: ");
            }
        });
    }
}
